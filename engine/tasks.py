from __future__ import absolute_import, unicode_literals
from noverde.celery import app
from celery.utils.log import get_task_logger
from datetime import date, datetime
from loan.models import LoanResult
from loan.loan import Loan
from .errors import MinimumAgeError, MinimumScoreError, CommitmentNotAchievedError
from .noverde import Noverde
import bisect
import math
import sys

logger = get_task_logger(__name__)

@app.task
def age_policy(loan):
  """
  Evaluates if the age policy is accomplished, minimum age for a loan is 18 years old

  Parameters
  ----------
  loan: LoanSerializer object
      Personal information and loan information for requesting a loan
  
  Raises
  ------
  MinimumAgeError
      If the minimum age is not reached
  """
  born = datetime.strptime(loan.get('birthday'), '%Y-%m-%dT%H:%M:%S')
  today = date.today()
  age = today.year - born.year - ((today.month, today.day) < (born.month, born.day))
  logger.info(f"age: {age}")
  if age < 18:
    Loan.refuseLoan(loan.get('id'), 'age')
    raise MinimumAgeError('Minimum age was not reached')
  return loan

@app.task
def score_policy(loan):
  """
  Evaluates if the score policy is accomplished, minimum score for a loan is 600

  Parameters
  ----------
  loan: LoanSerializer object
      Personal information and loan information for requesting a loan
  
  Raises
  ------
  MinimumAgeError
      If the minimum score is not reached
  """
  score = Noverde.getScore(loan.get('cpf'))
  if score < 600:
    Loan.refuseLoan(loan.get('id'), 'score')
    raise MinimumScoreError('Minimum score was not reached')
  return loan

terms = [6, 9, 12]
scores = [699, 799, 899, 1000]
interests = [
  [0.064, 0.066, 0.069],
  [0.055, 0.058, 0.061],
  [0.047, 0.050, 0.053],
  [0.039, 0.042, 0.045]
]

@app.task
def commitment_policy(loan):
  """
  Evaluates if the commitment policy is accomplished, evaluate if the monthly income is enought for the monthly quota

  Parameters
  ----------
  loan: LoanSerializer object
      Personal information and loan information for requesting a loan
  
  Raises
  ------
  CommitmentNotAchievedError
      If the minimum commitment is not achieved
  """
  commitment = Noverde.getCommitment(loan.get('cpf'))
  score = Noverde.getScore(loan.get('cpf'))

  if commitment and score:
    term_idx = terms.index(loan.get('terms'))
    score_idx = bisect.bisect(scores, score)
    rest_income = round(float(loan.get('income')) * (1. - commitment), 2)
    logger.info(f"rest_income: {rest_income}")

    quota = sys.maxsize
    amount = float(loan.get('amount'))
    term = None
    while rest_income < quota and term_idx < len(terms):
      term = terms[term_idx]
      interest = interests[score_idx][term_idx]
      
      sigma = math.pow((1. + interest), term)
      factor = (sigma * interest) / (sigma - 1.)
      
      quota = amount * round(factor, 2)
      logger.info({"amount": amount, "interest": interest, "terms": term, "quota" : round(quota, 2)})
      
      term_idx += 1

    if rest_income < quota:
      Loan.refuseLoan(loan.get('id'), 'commitment')
      raise CommitmentNotAchievedError('Commitment was not achieved')

    loanResult = LoanResult.objects.get(id=loan.get('id'))
    loanResult.amount = loan.get('amount')
    loanResult.terms = term
    loanResult.save()
  
  return loan

@app.task
def approve_loan(loan):
  """
  Aprove a loan

  Parameters
  ----------
  loan: LoanSerializer object
      Personal information and loan information for requesting a loan
  """
  Loan.approveLoan(loan.get('id'))