from django.test import TestCase
from unittest.mock import patch
from loan.models import LoanResult, Client
from .tasks import age_policy, score_policy, commitment_policy
from .errors import MinimumScoreError, MinimumAgeError, CommitmentNotAchievedError

class EngineTestCase(TestCase):
  """ Suite Test in order to test the credit engine """

  def setUp(self):
    """ Initial setup """
    Client.objects.create(cpf='23423423498', name='Teste', birthday='1992-02-29')
    LoanResult.objects.create(id='a1d0d8ae-e5f5-4e2c-8d67-8e315c52e9f1', status='processing', result=None, refused_policy=None, amount=None, terms=None, client_id='23423423498')
    return super().setUp()

  def testMinimumAgeError(self):
    """ Testing minimum age not accomplished """
    loanRequest = dict()
    loanRequest['id'] = 'a1d0d8ae-e5f5-4e2c-8d67-8e315c52e9f1'
    loanRequest['name'] = 'Teste'
    loanRequest['cpf'] = '23423423456'
    loanRequest['birthday'] = '2010-05-12T00:00:00'
    loanRequest['amount'] = '4000.00'
    loanRequest['terms'] = 6
    loanRequest['income'] = '1000.00'

    with self.assertRaises(MinimumAgeError): age_policy(loanRequest)

  @patch('engine.noverde.Noverde')
  def testMinimumScoreError(self, noverde):
    """ Testing minimum score not accomplished """
    noverde.getScore.return_value = 500

    loanRequest = dict()
    loanRequest['id'] = 'a1d0d8ae-e5f5-4e2c-8d67-8e315c52e9f1'
    loanRequest['name'] = 'Teste'
    loanRequest['cpf'] = '23423423456'
    loanRequest['birthday'] = '1988-05-12T00:00:00'
    loanRequest['amount'] = '4000.00'
    loanRequest['terms'] = 6
    loanRequest['income'] = '1000.00'

    with self.assertRaises(MinimumScoreError): score_policy(loanRequest)
  
  @patch('engine.noverde.Noverde')
  def testCommitmentNotAchievedError(self, noverde):
    """ Testing commitment not achieved """
    noverde.getCommitment.return_value = 0.8

    loanRequest = dict()
    loanRequest['id'] = 'a1d0d8ae-e5f5-4e2c-8d67-8e315c52e9f1'
    loanRequest['name'] = 'Teste'
    loanRequest['cpf'] = '23423423456'
    loanRequest['birthday'] = '1988-05-12T00:00:00'
    loanRequest['amount'] = '4000.00'
    loanRequest['terms'] = 6
    loanRequest['income'] = '100.00'
    
    with self.assertRaises(CommitmentNotAchievedError): commitment_policy(loanRequest)