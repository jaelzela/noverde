import requests
import logging
import json
from .errors import IntegrationError
from django.conf import settings

logger = logging.getLogger(__name__)

headers = dict()
headers["Content-Type"] = "application/json"
headers["x-api-key"] = settings.NOVERDE_API_KEY

SCORE_ENDPOINT = settings.NOVERDE_SCORE_ENDPOINT
COMMITMENT_ENDPOINT = settings.NOVERDE_COMMITMENT_ENDPOINT

class Noverde:
  """ Class for integration with third-party services, Noverde services """
  
  @staticmethod
  def getScore(cpf):
    """
    Retrieves the score for a person using his cpf

    Parameters
    ----------
    cpf: str
        CPF of the person
    
    Raises
    ------
    ValueError
        If cpf is None
    IntegrationError
        If an error happens during third-party service call
    """
    if cpf is None:
      raise ValueError

    data = dict()
    data["cpf"] = cpf

    res = requests.post(SCORE_ENDPOINT, data=json.dumps(data), headers=headers)
    if res.status_code == 200:
      result = res.json()
      score = result.get('score')
      logger.info(f"score: {score}")
      return score
    
    raise IntegrationError(f'Score service returns {res.json()}')
    
  @staticmethod
  def getCommitment(cpf):
    """
    Retrieves the commitment percentage for a person using his cpf

    Parameters
    ----------
    cpf: str
        CPF of the person
    
    Raises
    ------
    ValueError
        If cpf is None
    IntegrationError
        If an error happens during third-party service call
    """
    if cpf is None:
      raise ValueError

    data = dict()
    data["cpf"] = cpf

    res = requests.post(COMMITMENT_ENDPOINT, data=json.dumps(data), headers=headers)
    if res.status_code == 200:
      result = res.json()
      commitment = result.get('commitment')
      logger.info(f"commitment: {commitment}")
      return commitment
    
    raise IntegrationError(f'Commitment service returns {res.json()}')