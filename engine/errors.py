class Error(Exception):
   """Base class for exceptions"""
   pass

class MinimumAgeError(Error):
   """Raised when the minimum age is not reached"""
   pass

class MinimumScoreError(Error):
   """Raised when the minimum score is not reached"""
   pass

class CommitmentNotAchievedError(Error):
   """Raised when the commitment is not achieved"""
   pass

class IntegrationError(Error):
   """Raised when the response from third-party services is not successed"""
   pass