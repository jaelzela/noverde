from django.contrib import admin
from django.urls import path, include
from loan import views

urlpatterns = [
    path('loan', include('loan.urls')),
]
