FROM python:3.7-alpine
ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache \
    build-base \
    g++ \
    cairo-dev \
    jpeg-dev \
    pango-dev \
    giflib-dev \
    postgresql-dev \
    postgresql-client

RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
RUN apk add --no-cache bash
ADD . /code/
# RUN python manage.py migrate

# Start django script
COPY start /start
RUN sed -i 's/\r//' /start
RUN chmod +x /start

# Start celery worker script
COPY start-celeryworker /start-celeryworker
RUN sed -i 's/\r//' /start-celeryworker
RUN chmod +x /start-celeryworker