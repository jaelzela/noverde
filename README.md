# Noverde

Solução do desafio apresentado pela Noverde ([link](https://github.com/noverde/challenge#pol%C3%ADticas-de-cr%C3%A9dito))

**IMPORTANTE:** Considerações que foram seguidas do desafio
- As informações pessois dos clientes (cpf, nome, birthday) são informações que nunca vão mudar no passar do tempo, porem essas informações foram persistidas, assim caso uma solicitação de emprestimo seja solicitado por um cliente já cadastrado (seguno seu cpf), será recuperada as informações pre-existentes para continuar com a evalução.

## Instruções

### Requisitos

- docker~19.03.5
- docker-compose~1.25.4
- Criar o arquivo `.env` na pasta raiz do projeto com as seguintes variables

```properties
# Third-party services
NOVERDE_API_KEY=wertbasdsnfdgjfdjkd
NOVERDE_SCORE_ENDPOINT=https://mydomain.com/score
NOVERDE_COMMITMENT_ENDPOINT=https://mydomain.com/commitment
# Database
DB_HOST=mydomain.com
DB_PORT=21324
DB_PASSWORD=123456
DB_USER=usuario
DB_DATABASE=desafio
# Celery
CELERY_BROKER_URL=pyamqp://guest@mydomain.com//
CELERY_RESULT_BACKEND=amqp://guest@mydomain.com//
```
Note: valores ilustrativos, valores corretos serão passados por email

- **NOVERDE_API_KEY:** chave de autenticação para chamar aos serviços da Noverde
- **NOVERDE_SCORE_ENDPOINT:** endpoint do serviço de score
- **NOVERDE_COMMITMENT_ENDPOINT:** endpoint do serviço de score
- **DB_HOST:** host do banco de dados
- **DB_PORT:** porta de banco de dados
- **DB_PASSWORD:** senha do banco de dados
- **DB_USER:** usuario do bando de dados
- **DB_DATABASE:** nome do banco de dados
- **CELERY_BROKER_URL:** url de coneção ao broker
- **CELERY_RESULT_BACKEND:** url de coneção ao broker

### Execução

```ssh
docker-compose -f docker-compose.yml up -d
```

## Demo

O sistema se encontra no servidor `http://169.62.186.54` podemse chamar aos serviços diretamente nesse servidor. No repositorio existe um archivo Postman (`noverde.postman_collection`) que pode ser usado para testar os serviços.

### Serviços

* POST `/loan` ([mais detalhes](https://github.com/noverde/challenge#post-loan))
* GET `/loan/:id` ([mais detalhes](https://github.com/noverde/challenge#get-loanid))

## Observações

- CI/CD: foi configurado o CI/CD no gitlab que executa tres passos: Test, Release e Deploy.
  - Test: executa os testes implementados no projeto.
  - Release: cria automaticamento uma imagen do projecto e faz o upload no registro de contenedores, este passo é executado somente se o passo anterior foi executado com sucesso.
  - Deploy: sobe a ultima ver do projecto no servidor configurado (paso manual).
- Existem dois configurações para o docker compose: `docker-compose.yml`, `docker-compose-prod.yml`.
  - `docker-compose.yml` usado para a geração da imagem do projeto, também pode ser usado para execução do projeto en local.
  - `docker-compose-prod.yml` usado para o deploy no servidor.
