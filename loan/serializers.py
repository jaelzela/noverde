from rest_framework import serializers
from .loan import Loan

class LoanSerializer(serializers.Serializer):
  """ Serializer to evaluate input data received from a request call in Loan Requesting """
  id = serializers.CharField(required=False, allow_blank=True, max_length=36)
  name = serializers.CharField(required=True, allow_blank=False, max_length=120)
  cpf = serializers.CharField(required=True, allow_blank=False, max_length=11)
  birthday = serializers.DateField(required=True)
  amount = serializers.DecimalField(required=True, max_digits=6, decimal_places=2, max_value=4000, min_value=1000)
  terms = serializers.ChoiceField(required=True, choices=[6, 9, 12])
  income = serializers.DecimalField(required=True, max_digits=8, decimal_places=2)

  def create(self, validated_data):
    """ Create new Loan Request action after successful validations """
    return Loan.create(validated_data)