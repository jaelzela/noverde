from .models import LoanRequest, LoanResult, Client
import logging

logger = logging.getLogger(__name__)

class Loan:
  """ Loan class to manage with data layer """

  @staticmethod
  def create(data):
    """ 
    Create a loan request and a client if it does not exist in the database 
    
    Parameters
    ----------
    data: LoanSerializer object
        Information required for a loan evaluation
    """
    client = None
    if not Client.objects.filter(cpf=data.get('cpf')).exists():
      client = Client.objects.create(**{
        'cpf': data.get('cpf'),
        'name': data.get('name'),
        'birthday': data.get('birthday')
      })
      logger.info(f'Client created with cpf: {data.get("cpf")}')
    else:
      logger.info(f'Client with cpf {data.get("cpf")} already exist, retrieving information')
      client = Client.objects.get(cpf=data.get('cpf'))
    
    loan = LoanRequest.objects.create(**{
      'amount': data.get('amount'),
      'terms': data.get('terms'),
      'income': data.get('income'),
      'client': client
    })
    data.update(id = loan.id)
    logger.info(f'Loan request created: {data}')
    return data

  @staticmethod
  def findById(loanId):
    """ 
    Retrieve the loan result stored in database 
    
    Parameters
    ----------
    loanId: str
        Id of the LoanRequest
    """
    logger.info(f'Retrieving loan {loanId}')
    return LoanResult.objects.get(id=loanId)

  @staticmethod
  def refuseLoan(loanId, reason):
    """
    Update the required field for a refuse loan action
    
    Parameters
    ----------
    loanId: str
        Id of the LoanRequest
    reason: str
        Policy which was not reached
    """
    logger.info(f'Refusing loan {loanId} because of {reason}')
    loanResult = LoanResult.objects.get(id=loanId)
    loanResult.status = 'completed'
    loanResult.result = 'refused'
    loanResult.refused_policy = reason
    loanResult.save()
    return

  @staticmethod
  def approveLoan(loanId):
    """
    Update the required field for an approve loan action
    
    Parameters
    ----------
    loanId: str
        Id of the LoanRequest
    """
    logger.info(f'Approving loan {loanId}')
    loanResult = LoanResult.objects.get(id=loanId)
    loanResult.status = 'completed'
    loanResult.result = 'approved'
    loanResult.save()
    return