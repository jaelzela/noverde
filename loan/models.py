import uuid
from django.db import models

class Client(models.Model):
  """ Client model stores personal information for people requestiong a loan """
  cpf = models.CharField(primary_key=True, max_length=11, null=False)
  name = models.CharField(max_length=120, null=False)
  birthday = models.DateField(null=False)

class LoanRequest(models.Model):
  """ Loan Request model stores the main information in order to be evaluated """
  id = models.UUIDField(primary_key = True, default = uuid.uuid4)
  amount = models.DecimalField(max_digits=6, decimal_places=2)
  terms = models.IntegerField()
  income = models.DecimalField(max_digits=8, decimal_places=2)
  client = models.ForeignKey(Client, on_delete=models.CASCADE)

class LoanResult(models.Model):
  """ Loan Result model stores the main information of the result evaluation for a loan """
  id = models.UUIDField(primary_key = True, default = uuid.uuid4)
  status = models.CharField(max_length=120, default='processing')
  result = models.CharField(max_length=120, null=True, default=None)
  refused_policy = models.CharField(max_length=120, null=True, default=None)
  amount = models.DecimalField(max_digits=6, decimal_places=2, null=True)
  terms = models.IntegerField(null=True)
  client = models.ForeignKey(Client, on_delete=models.CASCADE)