from rest_framework import viewsets
from rest_framework.parsers import JSONParser
from django.http import HttpResponse, Http404, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from celery import chain
from .serializers import LoanSerializer
from .loan import Loan
from engine.tasks import age_policy, score_policy, commitment_policy, approve_loan
import logging

logger = logging.getLogger(__name__)

@csrf_exempt
def loan(request):
  """ View for create a new loan request """
  if request.method == 'POST':
    data = JSONParser().parse(request)
    logger.info(f'New loan request {data}')

    serializer = LoanSerializer(data=data)
    if serializer.is_valid():
      loan = serializer.save()
      result = chain(age_policy.s() | score_policy.s() | commitment_policy.s() | approve_loan.s())(loan)
      return JsonResponse({ 'id' : loan.get('id')}, status=201)
    else:
      logger.info(f'Validations were not accomplished {serializer.errors}')

    errors = []
    for key, value in serializer.errors.items():
      errors.append({key: value})
    
    return JsonResponse({ 'errors': errors }, status=400, safe=False)

  raise Http404

@csrf_exempt
def loanResult(request, id):
  """ View to retrieve the current state of a loan request """
  if request.method == 'GET':
    logger.info(f'Loan request result for {id}')
    loan = Loan.findById(id)
    return JsonResponse(model_to_dict(loan))
  
  raise Http404