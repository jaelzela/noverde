from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
  path(r'', views.loan, name='loan'),
  url(r'/([a-f\d]{8}-[a-f\d]{4}-4[a-f\d]{3}-[89ab][a-f\d]{3}-[a-f\d]{12})', views.loanResult, name='loan_result'),
]
