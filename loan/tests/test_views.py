from django.test import TestCase, Client
from django.urls import reverse
from loan.models import LoanResult, Client

class ViewsTestCase(TestCase):
  """ Suite Test in order to test views """

  def setUp(self):
    """ Initial setup """
    Client.objects.create(cpf='23423423498', name='Teste', birthday='1992-02-29')
    LoanResult.objects.create(id='a1d0d8ae-e5f5-4e2c-8d67-8e315c52e9f1', status='processing', result=None, refused_policy=None, amount=None, terms=None, client_id='23423423498')
    LoanResult.objects.create(id='a1d0d8ae-e5f6-4e2c-8d67-8e315c52e9f1', status='completed', result='approved', refused_policy=None, amount=4000, terms=9, client_id='23423423498')
    LoanResult.objects.create(id='a1d0d8ae-e5f7-4e2c-8d67-8e315c52e9f1', status='completed', result='refused', refused_policy='age', amount=None, terms=None, client_id='23423423498')
    LoanResult.objects.create(id='a1d0d8ae-e5f8-4e2c-8d67-8e315c52e9f1', status='completed', result='refused', refused_policy='score', amount=None, terms=None, client_id='23423423498')
    LoanResult.objects.create(id='a1d0d8ae-e5f9-4e2c-8d67-8e315c52e9f1', status='completed', result='refused', refused_policy='commitment', amount=None, terms=None, client_id='23423423498')
    return super().setUp()

  def testSuccessLoan(self):
    """ Testing a success loan request registration """
    client = Client()
    data = dict()
    data['name'] = 'Jael Zela'
    data['cpf'] = '12345678901'
    data['birthday'] = '2010-05-12'
    data['amount'] = 4000
    data['terms'] = 6
    data['income'] = 1000
    response = self.client.post(reverse('loan'), data=data, content_type="application/json")
    self.assertEqual(response.status_code, 201)
    loan = response.json()
    self.assertIsNotNone(loan['id'])

  def testValidationErrorLoan(self):
    """ Testing input data validations """
    client = Client()
    data = dict()
    data['name'] = 'Jael Zela'
    data['cpf'] = '12345678901'
    data['amount'] = 6000
    data['terms'] = 3
    data['income'] = 1000
    response = self.client.post(reverse('loan'), data=data, content_type="application/json")
    self.assertEqual(response.status_code, 400)
    errors = response.json()
    self.assertIsNotNone(errors['errors'])

  def testProcessingLoan(self):
    """ Testing loan request in progress evaluation """
    id = 'a1d0d8ae-e5f5-4e2c-8d67-8e315c52e9f1'
    status = 'processing'
    result = None
    refused_policy = None
    terms = None
    amount = None

    client = Client()
    response = self.client.get(reverse('loan_result', args=(id,)))
    self.assertEqual(response.status_code, 200)
    
    loanResult = response.json()
    self.assertEqual(loanResult['id'], id)
    self.assertEqual(loanResult['status'], status)
    self.assertEqual(loanResult['result'], result)
    self.assertEqual(loanResult['refused_policy'], refused_policy)
    self.assertEqual(loanResult['terms'], terms)
    self.assertEqual(loanResult['amount'], amount)

  def testApprovedLoan(self):
    """ Testing aproved loan request """
    id = 'a1d0d8ae-e5f6-4e2c-8d67-8e315c52e9f1'
    status = 'completed'
    result = 'approved'
    refused_policy = None
    terms = 9
    amount = '4000.00'

    client = Client()
    response = self.client.get(reverse('loan_result', args=(id,)))
    self.assertEqual(response.status_code, 200)
    
    loanResult = response.json()
    self.assertEqual(loanResult['id'], id)
    self.assertEqual(loanResult['status'], status)
    self.assertEqual(loanResult['result'], result)
    self.assertEqual(loanResult['terms'], terms)
    self.assertEqual(loanResult['amount'], amount)
  
  def testAgeRefusedLoan(self):
    """ Testing loan request refused because of age policy """
    id = 'a1d0d8ae-e5f7-4e2c-8d67-8e315c52e9f1'
    status = 'completed'
    result = 'refused'
    refused_policy = 'age'
    terms = None
    amount = None

    client = Client()
    response = self.client.get(reverse('loan_result', args=(id,)))
    self.assertEqual(response.status_code, 200)
    
    loanResult = response.json()
    self.assertEqual(loanResult['id'], id)
    self.assertEqual(loanResult['status'], status)
    self.assertEqual(loanResult['result'], result)
    self.assertEqual(loanResult['terms'], terms)
    self.assertEqual(loanResult['amount'], amount)

  def testScoreRefusedLoan(self):
    """ Testing loan request refused because of score policy """
    id = 'a1d0d8ae-e5f8-4e2c-8d67-8e315c52e9f1'
    status = 'completed'
    result = 'refused'
    refused_policy = 'score'
    terms = None
    amount = None

    client = Client()
    response = self.client.get(reverse('loan_result', args=(id,)))
    self.assertEqual(response.status_code, 200)
    
    loanResult = response.json()
    self.assertEqual(loanResult['id'], id)
    self.assertEqual(loanResult['status'], status)
    self.assertEqual(loanResult['result'], result)
    self.assertEqual(loanResult['terms'], terms)
    self.assertEqual(loanResult['amount'], amount)
  
  def testCommitmentRefusedLoan(self):
    """ Testing loan request refused because of commitment policy """
    id = 'a1d0d8ae-e5f9-4e2c-8d67-8e315c52e9f1'
    status = 'completed'
    result = 'refused'
    refused_policy = 'commitment'
    terms = None
    amount = None

    client = Client()
    response = self.client.get(reverse('loan_result', args=(id,)))
    self.assertEqual(response.status_code, 200)
    
    loanResult = response.json()
    self.assertEqual(loanResult['id'], id)
    self.assertEqual(loanResult['status'], status)
    self.assertEqual(loanResult['result'], result)
    self.assertEqual(loanResult['terms'], terms)
    self.assertEqual(loanResult['amount'], amount)